[TOC]

# 一、项目说明

1. 项目描述：【travel 旅游出行】是一款旅游出行类的 WebApp（H5）
2. 仓库地址：https://gitee.com/mrthinco/travel
3. 代码说明：
   - develop 分支为：开发分支，master 分支为：正式分支
   - 此项目为纯前端项目，不含后端、数据库等（方便本地运行、学习、复用）
4. 技术框架：前端：Vue2.x、Vant2.x、Axios、Sass...
5. 项目展示：![旅游出行](src/assets/profile/1.png)

# 二、目录说明

## 2.1 项目目录

    ├── build                      // 构建相关
    ├── bin                        // 执行脚本
    ├── public                     // 公共文件
    │   ├── favicon.ico            // favicon图标
    │   └── index.html             // html模板
    │   └── robots.txt             // 反爬虫
    ├── src                        // 源代码
    │   ├── api                    // 所有请求
    │   ├── assets                 // 主题 字体等静态资源
    │   ├── components             // 全局公用组件
    │   ├── directive              // 全局指令
    │   ├── layout                 // 布局
    │   ├── router                 // 路由
    │   ├── store                  // 全局 store管理
    │   ├── utils                  // 全局公用方法
    │   ├── views                  // view
    │   ├── App.vue                // 入口页面
    │   ├── main.js                // 入口 加载组件 初始化等
    │   ├── permission.js          // 权限管理
    │   └── settings.js            // 系统配置
    ├── .editorconfig              // 编码格式
    ├── .env.development           // 开发环境配置
    ├── .env.production            // 生产环境配置
    ├── .env.staging               // 测试环境配置
    ├── .eslintignore              // 忽略语法检查
    ├── .eslintrc.js               // eslint 配置项
    ├── .gitignore                 // git 忽略项
    ├── babel.config.js            // babel.config.js
    ├── package.json               // package.json
    └── vue.config.js              // vue.config.js

## 2.2 页面模块

页面按照模块进行分类放置到`views`中，接口也按照模块放置到`api`中，且将项目中的模块整理归档到这里，方便快速查找：

1.  user 用户模块
2.  order 订单模块
3.  analysis 统计模块

## 2.3 components 组件

1.  组件命名规范
    - 组件名称：小驼峰，尽量语义化，如：userList
    - 组件文件：组件名称/index.vue，一个组件对应一个文件夹，该文件夹下可以有多个类型的文件（如：js、img、css 等）。如：`userList/index.vue`
2.  组件存放规范
    - 自定义公共复用组件存放为：`components/BaseForm/index.vue` ，这里存放自定义复用组件，方便大家开发和复用、共享。
    - 自定义非公共组件存放到：`当前页/cpns`，因为有的页面组件，其他页面根部就不会复用到，只是为了让主页面代码简洁和便于维护，那么这些组件，应该直接放到当前页下的`cpns`文件夹中，独立维护。
3.  组件说明规范
    每个自定义组件，组件用法、参数描述应该简单、清晰，在组件代码的`<script>`标签内的第一行备注好：组件名称、组件描述、开发人员、组件参数等。
    ```JavaScript
    <script>
      /*
      * LoadMore 加载更多
      * @description 用于列表中，做滚动加载使用，展示 loading 的各种状态
      * @author 杰伦
      * @property {String} status = [more|loading|noMore] loading 的状态
      * 	@value more loading 前
      * 	@value loading loading 中
      * 	@value noMore 没有更多了
      * @property {Number} iconSize 指定图标大小
      */
    </script>
    ```

## 2.4 assets 静态资源

1.  `assets/font` 字体文件
2.  `assets/icon` 图标文件
3.  `assets/img` 图片文件
    - 建议图片能够放后端或者云存储的尽量就别放前端，减少包体积和优化加载速度 。
    - 图片如果是复用的图片，不属于某个页面独有的图片，直接放此目录。
    - 图片如果属于某个功能模块独有的，需要建立一个和模块名称同名的文件夹，再存于文件夹下。如：`assets/img/user` 用户模块的图片。
4.  `assets/style` 样式文件
    - 样式文件夹下，统一在`index.scss`中导入所有样式，然后在`main.js`中统一导入。
    - 按照 UI 设计稿，整理了一份基础样式库，放置于`@/assets/styles/base.scss`，里面包含基础主题颜色、基础样式，欢迎自取。

## 2.5 store vuex 缓存

我们采用分模块来管理各个 vuex 子模块，便于项目的维护和整合。

1.  `index.js` 此文件是入口文件，方便整合和引入 vuex 模块
2.  `modules/user.js` 此文件是用户信息相关 vuex 模块

# 三、命名规范

## 3.1 文件命名

`.vue/.css/.js` 文件命名采用小驼峰，尽量语义化，如：`userList.vue`

## 3.2 CSS 命名规范

- class、id 命名采用小写字母加中划线，尽量语义化，前缀是类型，后面是操作。如：提交按钮 `btn-submit`。

## 3.3 JS 命名规范

- js 函数命名采用小驼峰，尽量语义化，如：`parseStartTag`。

## 3.4 文件夹命名

- 在 views 中的文件夹，采用小驼峰
- 在 components 中的文件夹，采用大驼峰
- 其余文件夹，统一小驼峰

# 四、版本规范

- 版本名称：2.220815.1
- 版本号：22208151
- 从左往右数字命名说明：
  - 第一位：2，表示当前大迭代版本号：2 版本
  - 第二至七位：220815，表示当前的发版日期：22 年 08 月 15 日
  - 第八位：1-9，表示本次上线版本的上架审核次数，默认为 1，重新上架一次递增 1，最高为 9
  - 注意：版本号和版本名称是对应关系，版本名称用点号分割

# 五、代码规范

## 5.1 书写规范

EditorConfig + Prettier + ESLint

## 5.2 提交规范

项目中使用了`commitlint`，所以需要规范 `commit-msg`：

```
<!--注意：冒号后有空格-->
git commit -m <type>(optional scope): <description>

<!--举例-->
git commit -m "feat(): 新增debounce函数"
git commit -m "fix(server): send cors headers"
git commit -m "feat(blog): add comment section"
git commit -m "chore: run tests on travis ci"

<!--type 类别：用于表明本次提交做了那种类型的改动。-->
- feat：新增功能
- fix：缺陷修复
- perf：性能优化
- refactor：重构代码(既没有新增功能，也没有修复 bug)
- style：不影响程序逻辑的代码修改(代码风格样式等，没有改变代码逻辑)
- docs：文档更新
- build：项目构建系统(例如 glup，webpack，rollup 的配置等)的提交
- revert：回滚某个更早之前的提交
- chore：不属于以上类型的其他类型

optional scope：一个可选的修改范围。用于标识此次提交主要涉及到代码中哪个模块。
description：一句话描述此次提交的主要内容，做到言简意赅。
```

# 六、项目插件

1. normalize.css 规范浏览器样式

```js
npm install --save normalize.css
```

2. fastclick 解决移动端浏览器 300 毫秒点击延迟问题

```js
npm install fastclick --save
```

3. tailwindcss 样式库

```js
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p
```

4. vant2.x UI 组件库

```js
npm i vant@latest-v2 -S
// 在编译过程中将 import 的写法自动转换为按需引入的方式
npm i babel-plugin-import -D
```

5. axios api 请求工具

```js
npm i axios --save
```

6. better-scroll 滚动组件

```js
npm i better-scroll --save
```
