export default {
  changeCity(state, payload) {
    state.city = payload
    localStorage.city = payload
  },
}
