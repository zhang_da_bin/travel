export default {
  changeCity(context, payload) {
    context.commit('changeCity', payload)
  },
}
