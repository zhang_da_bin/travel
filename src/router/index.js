// 路由：根据网址的不同，返回不同的内容给用户

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/home/HomeView'),
  },
  {
    path: '/city',
    name: 'City',
    component: () => import('@/views/city/CityView'),
  },
  {
    path: '/detail',
    name: 'Detail',
    component: () => import('@/views/detail/DetailView'),
  },
]

const router = new VueRouter({
  routes,
  scrollBehavior(to, from, savedPosition) {
    // 当切换到新路由时：页面滚到顶部
    if (savedPosition) {
      return savedPosition
    } else {
      return { top: 0 }
    }
  },
})

export default router
