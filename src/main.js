import Vue from 'vue'
import App from './App.vue'
import router from './router' // 路由
import store from './store' // 状态存储
import '@/assets/css/index.scss' // CSS样式库
import fastClick from 'fastclick' // 解决移动端浏览器 300 毫秒点击延迟问题
import '@/utils/index.js' // JS工具库

Vue.config.productionTip = false
fastClick.attach(document.body)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
