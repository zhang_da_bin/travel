import Vue from 'vue'
// 导入vant组件
import { Button, Swipe, SwipeItem, Lazyload, Sticky, Search, Icon } from 'vant'
const cpns = [Button, Swipe, SwipeItem, Lazyload, Sticky, Search, Icon]
// 注册组件
for (const item of cpns) {
  Vue.use(item)
}
